all: DFA clean exe

DFA: DFA.o estados.o transiciones.o main.o
		g++ -std=c++11 DFA.o estados.o transiciones.o main.o -o DFA

DFA.o: DFA.cpp
		g++ -std=c++11 -c DFA.cpp

estados.o: estados.cpp
		g++ -std=c++11 -c estados.cpp

transiciones.o: transiciones.cpp
		g++ -std=c++11 -c transiciones.cpp

main.o: main.cpp
		g++ -std=c++11 -c main.cpp

clean: 
	rm -rf *.o 

exe:
	clear
	./DFA