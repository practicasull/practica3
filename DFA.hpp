#pragma once

#include <set>
#include <string>
#include <fstream>
#include <ostream>


#include "estados.hpp"

class DFA{

private:

	std::set<Estados> estados;		// coleccion de estados
	unsigned int estado_inicial;		// estado inicial
	unsigned int n_estados;		// numero de estados

public:

	// LEER DFA

	DFA(std::string nombrefichero, int& error);
	DFA(const DFA& dfa);
	~DFA();

	// IDENTIFICAR ESTADOS DE MUERTE
	void Estados_Muerte();

	// ANALIZAR CADENA
	void Analizar_Cadena();

	friend std::ostream& operator<<(std::ostream& os, const DFA& dfa);

};
