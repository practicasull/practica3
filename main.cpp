#include "DFA.hpp"

#include <fstream>
#include <stdlib.h>



int main (void){

	char op;
	std::string nombrefichero;

  	int error = 0;

  	std::cout << std::endl << "Introduzca el nombre del fichero: ";

	std::cin >> nombrefichero;

  	DFA A(nombrefichero, error);

  	if(error == 1) exit(0); //stdlib

  	while(op!='m' && op!='i' && op!='a' && op!='q') {

		std::cout << std::endl << "Simulador de DFA" << std::endl << std::endl;
		std::cout << "\tm. Mostrar DFA" << std::endl;
		std::cout << "\ti. Identificar Estados de Muerte" << std::endl;
		std::cout << "\ta. Analizar Cadena" << std::endl;
		std::cout << "\tq. Salir del Programa" << std::endl << std::endl;
		std::cout << "Seleccione una opción: ";

		std::cin >> op;

		while (op != 'l' && op != 'm' && op != 'i' && op != 'a' && op != 'q') {

			std::cout << "Opción no válida" << std::endl << std::endl;
			std::cout << "\tm. Mostrar DFA" << std::endl;
			std::cout << "\ti. Identificar Estados de Muerte" << std::endl;
			std::cout << "\ta. Analizar Cadena" << std::endl;
			std::cout << "\tq. Salir del Programa" << std::endl;
			std::cout << "Vuelva a seleccionar una opción: ";

			std::cin >> op;

		}


		if (op == 'm') {

			std::cout << A;
			op = 't';

		}

		if (op == 'i') {

			A.Estados_Muerte();
			op = 't';

		}

		if (op == 'a') {

			A.Analizar_Cadena();
			op = 't';

		}

		if (op == 'q') break;

	}
}
