#include "estados.hpp"

Estados::Estados(){}

Estados::Estados(const int& i, std::fstream& file)
{

	id_ = i;

	file >> aceptacion >> n_transiciones;

	for (int j=0;j<n_transiciones;++j) {

		transiciones_.insert(Transicion(j,file));

	}

}


Estados::Estados(const int& i):
id_(i)
{}

Estados::Estados(const Estados& E)
{
	this->id_ = E.getId_();
	this->n_transiciones = E.getN_transiciones();
	this->aceptacion = E.getAceptacion();
	this->transiciones_ = E.transiciones_;
}



Estados::~Estados(){}

unsigned int Estados::getAceptacion() const
{
	return aceptacion;
}

unsigned int Estados::getN_transiciones() const
{
	return n_transiciones;
}

unsigned int Estados::getId_() const
{
	return id_;
}

void Estados::comprobar_cadena(char i, int &x, int &y, bool &fallo)const
{

	for(int j=0;j<n_transiciones;j++){

		int aux;

		aux = obtener_siguiente(i);

		if(aux != -1){		// Si hay un caracter que no pertenece a la cadena, no continua

			y = aux;

			std::cout << x << "                " << i << "          " << y << std::endl;

			x = obtener_siguiente(i);

			break;

		}else{

			fallo = true;
		}
	}
}

unsigned int Estados::obtener_siguiente(const char& i) const
{
	int sig = -1;

	for (auto &k: transiciones_) {

		if (k.getCaracter() == i) {

			sig = k.getEstado_sig();
			break;

		}

	}

	return sig;
}


bool Estados::operator<(const Estados& rhs) const
{
	return id_ < rhs.id_;
}

bool Estados::operator>(const Estados& rhs) const
{
	return rhs < *this;
}

bool Estados::operator<=(const Estados& rhs) const
{
	return !(rhs < *this);
}

bool Estados::operator>=(const Estados& rhs) const
{
	return !(*this < rhs);
}

std::ostream& operator<<(std::ostream& os, const Estados& estados)
{

	os << estados.id_<< " " << estados.aceptacion << " " << estados.n_transiciones;
	for (auto &i: estados.transiciones_)
		os << i;

	return os;
}
