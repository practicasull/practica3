#include <cstring>
#include "DFA.hpp"

DFA::DFA(std::string nombrefichero, int &error){

	std::fstream file;
  	file.open(nombrefichero);
  
	if(!file.is_open()){
	    
	  std::cerr << "Error en la apertura del fichero" << std::endl;
	  error = 1;

	}
	  
	else{
	    
	 	error = 0;

		file >> n_estados;
		file >> estado_inicial;

		estados.clear();

		for(int i=0;i<n_estados;i++){

			int x;

			file >> x;

			estados.insert(Estados(x,file));
		}
	}
}

DFA::DFA(const DFA& dfa)
{
	this->estado_inicial = dfa.estado_inicial;
	this->n_estados = dfa.n_estados;
	this->estados = this->estados;
}


DFA::~DFA(){}

void DFA::Estados_Muerte(){

	std::cout << std::endl << "Los estados de muerte del DFA son:" << std::endl;

	std::cout << "{";

	int cnt = 0;

	for(auto &i: estados){

		bool aux = true;	// suponer que es un estado de muerte

		for(auto &j: i.transiciones_)

			if(j.getEstado_sig() != i.getId_())

				aux = false;

		if(aux){

			std::cout << i.getId_();

			if(cnt != n_estados-1)

				std::cout << ",";
		}
		cnt++;
	}

	std::cout << "}" << std::endl;
}

void DFA::Analizar_Cadena(){

	bool fallo = false; 	// en caso de que se introduzca un caracter que no pertenece al lenguaje

	std::string cadena;

	std::cout << std::endl << "Cadena de entrada: ";

	std::cin >> cadena;

    if ( cadena == "&" && (*estados.find(Estados(estado_inicial))).getAceptacion() == 1 ) {

        std::cout << "Cadena ACEPTADA" << std::endl;

    }
    else {

        std::cout << std::endl << "Estado Actual    Entrada    Siguiente estado" << std::endl;

        int tam;

        int x = estado_inicial;    // En la 1ª iteracion es el estado inicial, luego es el estado actual
        int y = 0;                // Estado siguiente

        tam = cadena.length();


        for (int i = 0; i < tam; ++i) {

            auto it = estados.find(Estados(x));

            (*it).comprobar_cadena(cadena[i], x, y, fallo);

            if (fallo) break;

        }

        if ((*estados.find(Estados(x))).getAceptacion() == 0 || fallo)

            std::cout << "Cadena NO ACEPTADA" << std::endl;

        else std::cout << "Cadena ACEPTADA" << std::endl;

    }

}


std::ostream& operator<<(std::ostream& os, const DFA& dfa)
{

	os << dfa.n_estados << std::endl;
	os << dfa.estado_inicial << std::endl;

	for (auto &i: dfa.estados)

		os << i << std::endl;

	return os;
}





























/*----------------------------------------------------------------------------------------

// COMPROBAR QUE NO HAY NINGUN SIMBOLO QUE NO PERTENEZCA AL ALFABETO

bool aux = true;

for(int i=0;i<tam;i++){	// TAMAÑO DE LA CADENA

    if(aux != true)

        break;

    else aux = false;

    for(int j=0;j<estados[x].n_transiciones;j++)

        if(estados[x].caracter[j] == cadena[i])

            aux = true;

}

if(aux == false)

    cout << "Cadena NO ACEPTADA porque contiene símbolos que no pertenecen al alfabeto" << endl;

else{


    for(int i=0;i<tam;i++){		// PARA CADA POSICION DE LA CADENA

        for(int j=0;j<estados[x].n_transiciones;j++)

            if(estados[x].caracter[j] == cadena[i]){

                y = estados[x].estado_sig[j];

                cout << x << "                " << cadena[i] << "          " << y << endl;

                x = estados[x].estado_sig[j];
                break;
            }
        }

    if(estados[x].aceptacion == 0)

        cout << "Cadena NO ACEPTADA" << endl;

    else cout << "Cadena ACEPTADA" << endl;

}
}

------------------------------------------------------------------------------------*/