#pragma once

#include "transiciones.hpp"
#include <fstream>
#include <set>
#include <ostream>

class Estados{

private:

	unsigned int aceptacion;		// es un estado de aceptacion?
	unsigned int n_transiciones;	// numero de transiciones
	unsigned int id_;

public:

	std::set<Transicion> transiciones_;

public:

	Estados(const int& i, std::fstream& fstream);
	Estados(const int& i);
	Estados(const Estados& E);
	Estados();
	~Estados();

	unsigned int getAceptacion() const;

	unsigned int getN_transiciones() const;

	unsigned int getId_() const;

	// COMPROBAR SI UNA CADENA ES ACEPTADA O NO

	void comprobar_cadena(char i, int &x, int &y, bool &fallo) const;

	unsigned int obtener_siguiente(const char& i) const;

	bool operator<(const Estados& rhs) const;

	bool operator>(const Estados& rhs) const;

	bool operator<=(const Estados& rhs) const;

	bool operator>=(const Estados& rhs) const;

	friend std::ostream& operator<<(std::ostream& os, const Estados& estados);
};